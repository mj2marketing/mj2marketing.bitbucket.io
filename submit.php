<?php
        $captcha;
        if(isset($_POST['g-recaptcha-response'])){
          $captcha=$_POST['g-recaptcha-response'];
        }

        if(!$captcha){
          echo '<h2>Please check the the captcha.</h2>';
          exit;
        }

        $secretKey = "6LdUCUgUAAAAAEYYDgKebIU5pVGN482uns9lWlr_";
        $ip = $_SERVER['REMOTE_ADDR'];
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);

        if(intval($responseKeys["success"]) !== 1) {
          echo '<h2>Please check the the captcha.</h2>';
        } else {
            

            header('HTTP/1.1 307 Temporary Redirect');
            header('Location: https://frc.filebound.com/process/html2eform.ashx');

        }
?>